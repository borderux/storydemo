# Welcome to StoryDemo

Do you ever have have the need to demonstrate your components to your design team or to your client?  
Sure you do.  This happens all the time in development.  Wouldn't it be nice to have an easy way to deploy a 
different Storybook to demonstrate your components and send your client a link to their Storybook so they
could login and take a look?  That's why we created StoryDemo.

The StoryDemo project is a lightweight deployment system for managing multi-tenant Storybooks
for all your different clients.  The goal is allow easy deployments of sample Storybooks for your clients to review with
a security system to prevent unauthorized access.   

Features of StoryDemo are the following
* Adding deployment of [Storybooks](https://storybook.js.org) to your different projects in a consistent way
* Configuring secure SSL connection over [CloudFlare](https://cloudflare.com) for secure connections
* Authorization and security provided by [Auth0](https://auth0.com) to prevent unauthorized access to your Storybooks
* Low-cost cloud storage using [Amazon S3 buckets](https://aws.amazon.com/s3)
* Example CI/CD using [Gitlab](https://gitlab.com) for easy deployment

The project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) since its
a common starting point for client-side single-page apps.  That said, the only real magic is in the deployment scripts and
authorization scripts that are provided, which are not dependent on React.

## Setting up your project

There are a number of steps you will need to follow to setup your StoryDemo system.

1. [Configure Amazon AWS](docs/CONFIG_AMAZON.md): This sets up your Amazon S3 storage to host the system cheaply and easily
2. [Configure Auth0](docs/CONFIG_AUTH0.md): Sets up [Auth0](https://auth0.com) to authorize clients to access the system
3. [Configure CloudFlare](docs/CONFIG_CLOUDFLARE.md): Setup [CloudFlare](https://cloudflare.com) as your secure website access and SSL
4. [Configure your StoryDemo system](docs/CONFIG_STORYDEMO.md): Setup your StoryDemo system
5. [Configure sub-projects](docs/CONFIG_SUBPROJECTS.md): Setup your separate client-based sub-projects

## How it Works

If you would like more information on how the system works, you can read [Storybook Integration Details](docs/STORYBOOK_INTEGRATION.md)

## Feedback / Issues

If you have feedback or suggestions, feel free to [open a new issue](https://gitlab.com/borderux/storydemo/issues).  
Hopefully you like the system and find it useful.
