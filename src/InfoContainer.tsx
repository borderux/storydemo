import React from 'react';
import HeaderLogo from './HeaderLogo';
import './InfoContainer.css';

const InfoContainer: React.FC = () => {
  return (
    <div className="InfoContainer">
      <HeaderLogo />
      <h1 className="title">
        Welcome to StoryDemo
      </h1>
      <p>
        You likely have reached this page in error. You should have a full link provided to you to access your projects storybook.
      </p>
      <p>
        If you would like more information on the StoryDemo system
      </p>
      <a href="https://gitlab.com/borderux/storydemo">Go to the Gitlab page</a>
      <span className="footer">
        Brought to you by the fine folks at
        <a className="border-link" href="https://borderux.com">BorderUX</a>
      </span>
    </div>
  );
}

export default InfoContainer;