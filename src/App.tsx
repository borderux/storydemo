import React from 'react';
import InfoContainer from './InfoContainer';
import './App.css';

const App: React.FC = () => {
  return (
    <div className="App">
      <InfoContainer />
    </div>
  );
}

export default App;
