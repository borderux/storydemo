import React from 'react';
import './HeaderLogo.css';

const HeaderLogo: React.FC = () => {
  return (  
    <div className="HeaderLogo">
      <img className="Logo slide-up-fade-in" alt="Logo" src="border-logo.png"></img>
    </div>
  );
}

export default HeaderLogo;