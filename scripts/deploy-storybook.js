/*
 *****************************************************************************************
 * Border UX Source File
 * Copyright (C) 2019 Border LLC
 *
 * Deployment script that uses the Amazon awscli to deploy Storybooks to your S3 bucket
 * The awscli must be installed and the AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY must
 * be set in your environment to deploy correctly.
 *
 * To configure the deployment, alter the package.json and include a storyDemoDeploy key
 * with the following
 *   storyDemoDeploy {
 *     name: The project name.  When deployed, the storybook can be found under /projects/<project name>/<storybook name>
 *           If no name is specified, the name from the package.json will be used.  This is superseded by the clientCode+projectCode,
 *           which will be used instead of they are both set.  If these are set, the path will be /projects/clientCode/projectCode/<storybook name>
 *           This is the preferred way to set the path, over the old way of using the name
 *     clientCode: The clients code.  Should be a 3 letter abbreviation, but can be any string you like
 *     projectCode: The project code.  Should be a numbered code like 01.  Can be any string you like
 *     s3BucketName: The name of the Amazon S3 bucket that storybook will be deployed to
 *     region: The Amazon region where the S3 bucket can be found
 *   }
 *
 * When deploying, you will need to provide a unique name for the storybook deployment. This can
 * be set in three ways:
 *   1. Env variable STORYBOOK_NAME.  This can be used if manually trigging a build from Gitlab pipeline
 *   2. Branch name starts with "storybook/".  For example, if you push a branch with the name "storybook/test",
 *       then the storybook name will be "test" and can be found under /projects/<project name>/test
 *   3. Command line argument "--name=".  Ex: "yarn deploy-storybook --name=test"
 *
 * [CHANGE HISTORY]
 * 1. 2019-Nov-18 (matt@borderux.com) File created
 * 2. 2020-May-20 (matt@borderux.com) Added clientCode+projectCode to set the project path
 *
 *****************************************************************************************
 */
const chalk = require("chalk");
const packageJson = require("../package.json");
const child_process = require("child_process");
const invalidNames = ["master", "stage"];

const STORYBOOK_STATIC_PATH = "storybook-static";
const PROJECTS_SUBFOLDER = "/projects/";
const SYNC_DELETE = true; // Set to true if the --delete option is used in syncing to cleanup unused files in the destination
const deployConfig = packageJson.storyDemoDeploy || packageJson.storyDemo || {};

function errorOut(error) {
  console.error(chalk.red("error ") + error);
  process.exit(1);
}

function infoOut(info) {
  console.log(chalk.blue("info ") + info);
}

function commandOut(command) {
  console.log(chalk.grey("  > " + command));
}

function getArguments() {
  const argv = process.argv;
  const result = {};
  if (!argv || !argv.length) {
    return result;
  }
  for (let i = 0, len = argv.length; i < len; i++) {
    const item = argv[i];
    if (item.indexOf("=") !== -1) {
      let command = item.split("=")[0];
      const value = item.split("=")[1];
      if (command.indexOf("--") === 0) {
        command = command.substr(2);
      }
      result[command] = value.trim();
    }
  }
  return result;
}

function gitCurrentBranchName() {
  if (process.env.CI_COMMIT_REF_NAME) {
    return process.env.CI_COMMIT_REF_NAME;
  }
  const gitCommand = "git rev-parse --abbrev-ref HEAD";
  let branchName = child_process.execSync(gitCommand, {
    encoding: "utf8",
  });
  if (branchName) {
    branchName = branchName.trim();
  } else {
    branchName = "";
  }
  return branchName === "HEAD" ? "" : branchName;
}

function getAppName() {
  // First we try to get the client and project code
  if (deployConfig.clientCode && deployConfig.projectCode) {
    return `${deployConfig.clientCode}${deployConfig.projectCode}`;
  }
  let name = deployConfig.name;
  if (!name) {
    name = packageJson.name;
  }
  if (!name) {
    errorOut(
      "package.json storyDemoDeploy must contain an application name for proper deployment"
    );
  }
  return name;
}

function getS3BucketName() {
  if (!deployConfig.s3BucketName) {
    errorOut("package.json storyDemoDeploy must contain an s3BucketName");
  }
  return deployConfig.s3BucketName;
}

function getArgDeploymentName() {
  return getArguments().name;
}

function getEnvDeploymentName() {
  return process.env.STORYBOOK_NAME;
}

function getGitDeploymentName() {
  let name = gitCurrentBranchName();
  const index = name.indexOf("storybook/");
  if (index !== -1) {
    name = name.substr(index + 10);
  } else {
    errorOut(
      'Branch name must start with "storybook/" to be considered a proper name"'
    );
  }
  return name;
}

function getDeploymentName() {
  const args = getArguments();
  let name = getArgDeploymentName();
  if (!name) {
    name = getEnvDeploymentName();
  }
  if (!name) {
    name = getGitDeploymentName();
  }
  if (invalidNames.find((item) => item === name) && !args.force) {
    errorOut("Invalid name for deployment name: " + name);
  }
  return name;
}

function getDeploymentPath() {
  return (
    getS3BucketName() +
    PROJECTS_SUBFOLDER +
    getAppName() +
    "/" +
    getDeploymentName()
  );
}

function getEnvVariables() {
  const env = process.env;
  if (env.CI) {
    if (!env.AWS_ACCESS_KEY_ID) {
      errorOut("Missing env.AWS_ACCESS_KEY_ID");
    }
    if (!env.AWS_SECRET_ACCESS_KEY) {
      errorOut("Missing env.AWS_SECRET_ACCESS_KEY");
    }
    if (!env.AWS_DEFAULT_REGION) {
      if (!deployConfig.region) {
        errorOut(
          "Missing region in package.json storybookDeploy configuration"
        );
      }
      env.AWS_DEFAULT_REGION = deployConfig.region;
    }
  } else {
    infoOut(
      "Assuming awscli is properly configured with Access Key and region"
    );
  }
  return env;
}

function deploy() {
  const deploymentPath = getDeploymentPath();
  const env = getEnvVariables();
  infoOut("Deploying to: https://" + deploymentPath);
  const command =
    "aws s3 sync " +
    STORYBOOK_STATIC_PATH +
    " s3://" +
    deploymentPath +
    (SYNC_DELETE ? " --delete" : "");
  commandOut(command);
  try {
    child_process.execSync(command, { stdio: "inherit", env: env });
    console.log(
      chalk.green("success") +
        " Deployed to: " +
        chalk.yellow("https://" + deploymentPath)
    );
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
}

deploy();
