/*
 *****************************************************************************************
 * Border UX Source File
 * Copyright (C) 2019 Border LLC
 *
 * Deployment script that uses the Amazon awscli to deploy StoryDemo to your S3 bucket
 * The awscli must be installed and the AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY must
 * be set in your environment to deploy correctly.
 *
 * To configure the deployment, alter the package.json and include a storyDemoDeploy key
 * with the following
 *   storyDemoDeploy {
 *     name: The project name.  When deployed, the storybook can be found under /projects/<project name>/<storybook name>
 *           If no name is specified, the name from the package.json will be used
 *     s3BucketName: The name of the Amazon S3 bucket that storybook will be deployed to
 *     region: The Amazon region where the S3 bucket can be found
 *   }
 *
 * [CHANGE HISTORY]
 * 1. 2019-Nov-18 (matt@borderux.com) File created
 *
 *****************************************************************************************
 */
const chalk = require("react-dev-utils/chalk");
const packageJson = require("../package.json");
const child_process = require("child_process");

const SOURCE_FOLDER = "build";
const deployConfig = packageJson.storyDemoDeploy || packageJson.storyDemo || {};

function errorOut(error) {
  console.error(chalk.red("error ") + error);
  process.exit(1);
}

function infoOut(info) {
  console.log(chalk.blue("info ") + info);
}

function commandOut(command) {
  console.log(chalk.grey("  > " + command));
}

function getArguments() {
  const argv = process.argv;
  const result = {};
  if (!argv || !argv.length) {
    return result;
  }
  for (let i = 0, len = argv.length; i < len; i++) {
    const item = argv[i];
    if (item.indexOf("=") !== -1) {
      let command = item.split("=")[0];
      const value = item.split("=")[1];
      if (command.indexOf("--") === 0) {
        command = command.substr(2);
      }
      result[command] = value.trim();
    }
  }
  return result;
}

function getS3BucketName() {
  if (!deployConfig.s3BucketName) {
    errorOut("package.json storyDemoDeploy must contain an s3BucketName");
  }
  return deployConfig.s3BucketName;
}

function getDeploymentPath() {
  return getS3BucketName();
}

function getEnvVariables() {
  const env = process.env;
  if (env.CI) {
    if (!env.AWS_ACCESS_KEY_ID) {
      errorOut("Missing env.AWS_ACCESS_KEY_ID");
    }
    if (!env.AWS_SECRET_ACCESS_KEY) {
      errorOut("Missing env.AWS_SECRET_ACCESS_KEY");
    }
    if (!env.AWS_DEFAULT_REGION) {
      if (!deployConfig.region) {
        errorOut(
          "Missing region in package.json storyDemoDeploy configuration"
        );
      }
      env.AWS_DEFAULT_REGION = deployConfig.region;
    }
  } else {
    infoOut(
      "Assuming awscli is properly configured with Access Key and region"
    );
  }
  return env;
}

function getSourceFolder() {
  const args = getArguments();
  return args.src || SOURCE_FOLDER;
}

function deploy() {
  const deploymentPath = getDeploymentPath();
  const env = getEnvVariables();
  infoOut("Deploying to: " + deploymentPath);
  const command =
    "aws s3 sync " + getSourceFolder() + " s3://" + deploymentPath;
  commandOut(command);
  try {
    child_process.execSync(command, { stdio: "inherit", env: env });
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
}

deploy();
