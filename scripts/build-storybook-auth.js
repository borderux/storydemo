/*
 *****************************************************************************************
 * Border UX Source File
 * Copyright (C) 2019 Border LLC
 *
 * Parses storybooks html output file and builds the minimized auth.js for output
 * This script does the following:
 *   > Parses the Storybook HTML output to remove <script> tags so they don't get loaded
 *     It puts the extracted information in a file called script-tags.json which is used
 *     in the auth.js script as an import
 *   > Runs the auth.js file through webpack to minimize it and include config json files
 *     so they don't have to be loaded separately
 *
 * You can alter the scripts behavior with the following arguments
 *   --dev={true|false}: Specifies development behavior. Turns off authorization for local development
 *   --authDir={string}: Specify the folder that contains the auth.js file
 *   --storybookDir={string}: Specify the folder of the Storybook output to parse
 *   --authFilename={string}: Specify the auth.js filename
 *   --authConfig={string}: Specify the filename of the auth config file. By default, this would be part
 *      of your package.json under the "storyDemo" key, but if you want to handle this externally
 *      then you can specify a path to an external JSON file
 *
 * [CHANGE HISTORY]
 * 1. 2019-Nov-22 (matt@borderux.com) File created
 *
 *****************************************************************************************
 */

const chalk = require("chalk");
const fs = require("fs");
const path = require("path");
const webpack = require("webpack");
const cheerio = require("cheerio");
const packageJson = require("../package.json");

const DEFAULT_STORYBOOK_DIR = "storybook-static";
const DEFAULT_AUTH_DIR = "scripts";
const DEFAULT_AUTH_FILENAME = "auth.js";
const STORYBOOK_MAIN_HTML = "index.html";
const DEV_AUTH_JS = false; // If set to true, the auth.js will not be minimized.  Only for testing purposes

const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = (relativePath) => path.resolve(appDirectory, relativePath);

const config = {
  devMode: false,
  storybookDir: DEFAULT_STORYBOOK_DIR,
  authDir: DEFAULT_AUTH_DIR,
  authFilename: DEFAULT_AUTH_FILENAME,
  authConfig: {},
  webpack: {
    mode: "production",
    bail: true,
    entry: resolveApp(path.join(DEFAULT_AUTH_DIR, DEFAULT_AUTH_FILENAME)),
    output: {
      path: resolveApp(DEFAULT_STORYBOOK_DIR),
      filename: DEFAULT_AUTH_FILENAME,
    },
    optimization: {
      minimize: true,
    },
    plugins: [],
  },
};
const scriptTagsOutput = {
  main: [],
  iframe: [], // iframe is no longer parsed because it was causing problems when loading storybook
};

// This is the injected head style tag to hide the screen behind white.  This is injected into the storybook html
const headStyleTag = `
<style>
  body::after {
    content: "";
    display: block;
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background-color: white;
  }
  .no-overlay::after {
    content: none !important;
  }
</style>`;

// These are additional script tags that are injected into the storybook head
const headAuth0ScriptTag = `<script src="https://cdn.auth0.com/js/auth0-spa-js/1.2/auth0-spa-js.production.js"></script>`;
const headAuthTag = `<script src="auth.js"></script>`;
const headInjects = [headStyleTag, headAuth0ScriptTag, headAuthTag];

function errorOut(error) {
  console.error(chalk.red("error ") + error);
  process.exit(1);
}

function infoOut(info) {
  console.log(chalk.blue("info ") + info);
}

function getArguments() {
  const argv = process.argv;
  const result = {};
  if (!argv || !argv.length) {
    return result;
  }
  for (let i = 0, len = argv.length; i < len; i++) {
    const item = argv[i];
    if (item.indexOf("=") !== -1) {
      let command = item.split("=")[0];
      const value = item.split("=")[1];
      if (command.indexOf("--") === 0) {
        command = command.substr(2);
      }
      result[command] = value.trim();
    }
  }
  return result;
}

function getAuthConfig() {
  if (packageJson && packageJson.storyDemo) {
    return packageJson.storyDemo;
  }
  const args = getArguments();
  if (!args.authConfig) {
    return null;
  }
  const configFile = fs.readFileSync(resolveApp(args.authConfig));
  if (!configFile) {
    return null;
  }
  try {
    return JSON.parse(configFile);
  } catch {
    return null;
  }
}

function parseAuthConfig(authConfig) {
  const config = {};
  const missing = [];
  const getParam = (param, required = true) => {
    if (authConfig[param] === undefined && required) {
      missing.push(param);
    } else {
      config[param] = authConfig[param];
    }
  };
  getParam("domain");
  getParam("clientId");
  getParam("disabled");
  getParam("customRuleDomain", false);
  getParam("accessRoleId", false);
  getParam("disabled", false);
  getParam("authorized", false);
  if (missing.length) {
    errorOut("Auth config is missing required fields: ", missing);
  }
  return config;
}

function processArguments() {
  const args = getArguments();
  const authDir = args.authDir || DEFAULT_AUTH_DIR;
  const authFilename = args.authFilename || DEFAULT_AUTH_FILENAME;
  let storybookDir = args.storybookDir || DEFAULT_STORYBOOK_DIR;
  config.devMode = !!args.dev;
  if (config.devMode) {
    storybookDir = "public";
  }
  config.storybookDir = storybookDir;
  config.authDir = authDir;
  config.authFilename = authFilename;
  config.webpack.mode = "production";
  config.webpack.entry = resolveApp(path.join(authDir, authFilename));
  config.webpack.output.path = resolveApp(storybookDir);
  config.webpack.output.filename = authFilename;
  config.webpack.optimization.minimize = true;
  if (config.devMode) {
    infoOut("Development mode");
  }
  if (DEV_AUTH_JS) {
    config.webpack.mode = "development";
    config.webpack.optimization.minimize = false;
  }
  infoOut("Auth input: " + config.webpack.entry);
  infoOut(
    "Auth output: " +
      path.join(config.webpack.output.path, config.webpack.output.filename)
  );
  const authConfig = getAuthConfig();
  if (!authConfig) {
    errorOut(
      "No auth configuration found. You must specify one in your package.json or specify the --authConfig= parameter"
    );
  }
  config.authConfig = parseAuthConfig(authConfig);
}

function getWebpackConfig() {
  const newConfig = Object.assign({}, config.webpack);
  // Insert our global variables using the define plugin
  newConfig.plugins.push(
    new webpack.DefinePlugin({
      SCRIPT_TAGS: JSON.stringify(scriptTagsOutput),
      AUTH_CONFIG: JSON.stringify(config.authConfig),
    })
  );
  return newConfig;
}

function readHtmlFile(filename) {
  const html = fs.readFileSync(filename, "utf8");
  return cheerio.load(html);
}

function writeHtmlFile(filename, data) {
  return fs.writeFileSync(filename, data, "utf8");
}

/**
 * Delete the assets-manifest to prevent someone from seeing it
 */
function deleteAssetManifest() {
  const filename = resolveApp(
    path.join(config.storybookDir, "asset-manifest.json")
  );
  infoOut("Deleting assets manifest file");
  try {
    fs.unlinkSync(filename);
  } catch (err) {
    // Do nothing
  }
}

function build() {
  const config = getWebpackConfig();
  infoOut("Building auth file");
  return new Promise((resolve, reject) => {
    webpack(config).run((err, stats) => {
      if (err) {
        reject(err);
        return;
      }
      resolve();
    });
  });
}

function getStorybookHtml() {
  return resolveApp(path.join(config.storybookDir, STORYBOOK_MAIN_HTML));
}

function parseBodyScriptTags(html) {
  const body = html("body");
  const scripts = body.find("script");
  const scriptSrcs = [];
  scripts.each(function () {
    const src = html(this).attr("src");
    if (src) {
      scriptSrcs.push({ src });
      html(this).remove();
    }
  });
  return scriptSrcs;
}

function injectHeader(html) {
  const head = html("head");
  headInjects.forEach((tag) => {
    head.append(tag);
  });
}

function parseMainHtml() {
  const filename = getStorybookHtml();
  infoOut("Parsing main html: " + filename);
  const html = readHtmlFile(filename);
  scriptTagsOutput.main = parseBodyScriptTags(html);
  injectHeader(html);
  writeHtmlFile(filename, html.html());
}

function disableAuth() {
  infoOut("*** Auth is disabled in auth_config.json ***");
  const filename = resolveApp(
    path.join(config.webpack.output.path, config.webpack.output.filename)
  );
  fs.writeFileSync(
    filename,
    'window.onload = function() { document.body.classList.add("no-overlay"); }',
    "utf-8"
  );
}

function main() {
  infoOut("Running build-storybook-auth");
  processArguments();
  if (config.authConfig.disabled || config.devMode) {
    disableAuth();
    return;
  }
  parseMainHtml();
  build().then(deleteAssetManifest).catch(errorOut);
}

main();
