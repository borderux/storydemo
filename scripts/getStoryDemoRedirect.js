/**
 * Extracts the StoryDemo redirect path from window.location.search or
 * the search string that you specify
 * @param {string} search: Search string to search for the rredirectPath parameter
 * @returns {string | undefined}: Returns undefined if the param is not found, or the path to redirect to if found
 */
function getStoryDemoRedirect(search) {
  var newPath = search || window.location.search;
  if (!newPath) {
    return undefined;
  }
  var index = newPath.indexOf("rredirectPath=");
  if (index === -1) {
    return undefined;
  }
  newPath = newPath.substr(index + 14, newPath.length);
  index = newPath.indexOf("&");
  if (index !== -1) {
    newPath = newPath.substr(0, index);
  }
  if (!newPath) {
    return "/";
  }
  if (newPath[0] !== "/") {
    return "/" + newPath;
  }
  return newPath;
}

export default getStoryDemoRedirect;
