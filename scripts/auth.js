/* eslint-disable no-undef */
/*
 *****************************************************************************************
 * Border UX Source File
 * Copyright (C) 2019 Border LLC
 *
 * Authorization script run in the head of Storybook.  Works with Auth0 SPA universal login
 *
 * To configure auth behavior, a global variable called AUTH_CONFIG must have the following:
 *   > domain: The Auth0 app domain
 *   > clientId: The Auth0 app clientId
 *   > customRuleDomain: When using user roles to authorize users, this is the rule domain
 *       that the role key will be found in the user object
 *   > accessRoleId: The projects role id.  This should be changed for each project
 *   > disabled: You can disable auth security if set to false.  This does not apply to
 *       this script, but alters the behavior of the build_auth.js build script
 *   > authorized: An array of emails for authorized users.  You can put authorized users
 *       here or use role based authorization
 *
 * This file was written in ES2015 Javascript to ensure you don't need to run a parser
 * like Babel to transform the file.
 *
 * [CHANGE HISTORY]
 * 1. 2019-Nov-19 (matt@borderux.com) File created
 * 2. 2020-May-21 (matt@borderux.com) Moved config and script tags to GLOBAL variables
 *
 *****************************************************************************************
 */

var auth0;
var _queryString;
var config = AUTH_CONFIG; // This is injected by webpack as part of the build process
var scriptTags = SCRIPT_TAGS || []; // This is injected by webpack as part of the build process

/**
 * Creates an error object so we can use it later
 * @param {any} content: The error content
 * @param {string} title: Friendly title to show in the browser
 */
function createError(content, title) {
  return {
    error: content,
    title: title,
    type: "error",
  };
}

function isError(error) {
  if (typeof error === "object") {
    return error.type === "error";
  }
  return false;
}

function setCookie(name, value, days) {
  var expires = "";
  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
    expires = "; expires=" + date.toUTCString();
  }
  document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

function eraseCookie(name) {
  document.cookie = name + "=; Max-Age=-99999999;";
}

function createClient() {
  return createAuth0Client({
    domain: config.domain,
    client_id: config.clientId,
  }).then(function (client) {
    auth0 = client;
  });
}

function checkAuthenticated() {
  return auth0.isAuthenticated();
}

function login() {
  // We need to store the location we want to redirect to as a cookie after login occurs
  setCookie(
    "storydemo-redirect-url",
    window.location.origin + window.location.pathname,
    10
  );
  // Now call the loginWithRedirect
  return auth0.loginWithRedirect({
    redirect_uri: window.location.origin,
  });
}

/**
 * Attempts a login if the user is not authenticated
 * @param {boolean} authenticated: Set to true if user is already authenticated
 */
function loginIfNotAuthenticated(authenticated) {
  if (authenticated) {
    return true;
  }
  if (_queryString.includes("error=")) {
    // There was an error in authentication
    throw createError(
      "Invalid query string: " + _queryString,
      "Authentication error"
    );
  }
  // Check the stored query string to see if it has the Auth0 authentication params
  // If so, we need to parse them and send them to the auth0 client
  if (_queryString.includes("code=") && _queryString.includes("state=")) {
    var newParams = _queryString;
    if (_queryString.includes("?")) {
      newParams = _queryString.replace("?", "&");
    }
    // This little trick is necessary since the Auth0 library accesses the window.location.search
    // to parse the params itself, which is unfortunate since they will have changed by the time
    // that this code is reached by Storybook.  This forces the params back into the URL so Auth0
    // can parse it.
    var currentUrl = window.location.href;
    window.history.replaceState({}, document.title, currentUrl + newParams);
    return auth0.handleRedirectCallback().then(function () {
      window.history.replaceState({}, document.title, currentUrl);
      return true;
    });
  }
  return login();
}

function checkRoles(user) {
  if (!user) {
    return false;
  }
  // Check if auth_config.json contains a customRuleDomain and accessRoleId to
  // use role based permissions
  if (!config.customRuleDomain || !config.accessRoleId) {
    return false;
  }
  var key = config.customRuleDomain + "/roles";
  if (!user[key] || !user[key].length) {
    return false;
  }
  for (var i = 0, len = user[key].length; i < len; i++) {
    if (user[key][i] === config.accessRoleId) {
      return true;
    }
  }
  return false;
}

function checkAuthConfigAuthorized(user) {
  if (!user) {
    return false;
  }
  if (config.authorized && config.authorized.length) {
    for (var i = 0, len = config.authorized.length; i < len; i++) {
      var authorizedItem = config.authorized[i]
        ? config.authorized[i].toLowerCase()
        : "";
      var testRegExp = new RegExp(authorizedItem);
      if (authorizedItem && testRegExp.test(user.email.toLowerCase())) {
        return true;
      }
    }
  }
  return false;
}

function checkUserAuthorized(user) {
  if (!user.email_verified) {
    throw createError(
      "Users email is not verified",
      "Please verify your email to be authorized"
    );
  }
  if (checkAuthConfigAuthorized(user)) {
    return true;
  }
  if (checkRoles(user)) {
    return true;
  }
  return false;
}

/**
 * Checks the user access against our list of authorized people found in the auth_config.json
 * @param {boolean} authenticated: Set to true if the user is already authenticated
 */
function checkUserAccess(authenticated) {
  if (!authenticated) {
    return false;
  }
  return auth0.getUser().then(function (user) {
    if (checkUserAuthorized(user)) {
      return true;
    }
    throw createError(
      "User is not authorized for this application",
      "You must be approved to access this application"
    );
  });
}

/**
 * We need to store the query string immediately when the page loads because storybook will
 * change it very quickly later
 */
function storeQueryString() {
  _queryString = window.location.search;
}

function printError(error) {
  error = isError(error) ? error : { error: error };
  if (error.error) {
    console.error(error.error);
  }
  if (error.title === undefined) {
    error.title = "An error has occurred";
  }
  if (!error.title) {
    return;
  }
  var elem = document.body;
  var errorElem = document.createElement("h1");
  errorElem.innerHTML = error.title;
  elem.appendChild(errorElem);
}

/**
 * Removes the error pseudo:after so storybook can be seen
 */
function removeOverlay(authorized) {
  if (!authorized) {
    return;
  }
  document.body.classList.add("no-overlay");
}

function addScriptTags(tags, element) {
  if (!tags || !tags.length) {
    return;
  }
  for (var i = 0, len = tags.length; i < len; i++) {
    var elem = document.createElement("script");
    elem.setAttribute("type", "text/javascript");
    if (tags[i].src) {
      elem.setAttribute("src", tags[i].src);
    } else {
      elem.setAttribute("text", tags[i].text);
    }
    element.appendChild(elem);
  }
}

function addMainScriptTags(authorized) {
  if (!authorized) {
    return false;
  }
  if (!scriptTags.main || !scriptTags.main.length) {
    return true;
  }
  addScriptTags(scriptTags.main, document.body);
  return true;
}

function getIFrameElement() {
  return new Promise(function (resolve, reject) {
    var numLoops = 0;
    var intervalId = setInterval(function () {
      var iFrame = document.getElementById("storybook-preview-iframe");
      if (iFrame) {
        if (iFrame.contentDocument) {
          iFrame = iFrame.contentDocument.getElementsByTagName("body")[0];
        } else if (iFrame.contentWindow) {
          iFrame = iFrame.contentWindow.document.getElementsByTagName(
            "body"
          )[0];
        }
        if (iFrame) {
          clearInterval(intervalId);
          resolve(iFrame);
          return;
        }
      }
      numLoops++;
      if (numLoops > 50) {
        clearInterval(intervalId);
        reject("Unable to find storybook iframe");
        return;
      }
    }, 100);
  });
}

function addIFrameScriptTags(authorized) {
  if (!authorized) {
    return false;
  }
  if (!scriptTags.iframe || !scriptTags.iframe.length) {
    return true;
  }
  return getIFrameElement().then(function (iFrame) {
    addScriptTags(scriptTags.iframe, iFrame);
    return true;
  });
}

function handleError(error) {
  removeOverlay(true);
  printError(error);
  // Remove the storybook root element so it can't be accessed
  document.getElementById("root").remove();
}

if (config) {
  window.onload = function () {
    return createClient()
      .then(checkAuthenticated)
      .then(loginIfNotAuthenticated)
      .then(checkUserAccess)
      .then(addMainScriptTags)
      .then(addIFrameScriptTags)
      .then(removeOverlay)
      .catch(handleError);
  };
} else {
  printError("Undefined configuration");
}
storeQueryString();
