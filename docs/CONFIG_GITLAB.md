# Configuring GitLab

[GitLab](https://gitlab.com) is an excellent continuous integration (CI) and continuous delivery (CD) system that we chose
to host this project.  It has an excellent free tier that lets you do quite a bit more than just using a system such as Github.

This project includes a sample [.gitlab-ci.yml](../.gitlab-ci.yml) to setup GitLab for continuous deployment.  The most important
job is the `storybook-deploy` job/pipeline.  It is configured for the following:

1. In your sub-project, if you create a branch that starts with `storybook/` and push it to GitLab, it will assume you want to deploy a Storybook instance to your StoryDemo system.  For example, if you create a branch called `storybook/test`, GitLab will deploy a new Storybook instance to the folder `projects/<name>/test`, where name is the name configured in [package.json](../package.json)

2. If you got `CI / CD > Pipelines` and select `Run Pipeline`, you can specify an environment variable called `STORYBOOK_NAME` and it
will use the name specified as the Storybook instance name you want to deploy to.  

You are asking, why do I need to setup GitLab to do this, rather than just configure the Amazon CLI and just deploy from my own terminal?  The reason for this is that you can avoid having to set this up on all developers systems and providing them with the 
secret Amazon deployment information.  Deployment can be managed through GitLab and tracked easily.  Its also a lot easier for 
your developers, since they don't have to think about it. 

## Setup Amazon AWS in GitLab

In order for GitLab to deploy your system to your Amazon S3 bucket, you need to configure the Amazon CLI to allow GitLab access.  

1. **Create a new IAM User**: You will need to create a new user.  Select the `IAM` service and select `Add User`
2. Give it a friendly name such as *gitlab-s3-deployer* and select the AWS access type to `Programmatic access`
3. Once the user is created, Amazon will provide the `ACCESS KEY ID` and `SECRET ACCESS KEY`.  You will want to save these (for now).
3. For `Permissions`, you need to grant it read/write access to your S3 bucket ARN you created in [Configuring Amazon](CONFIG_AMAZON.md).  
```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "s3:PutObject",
                "s3:GetObject",
                "s3:ListBucket",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::<BUCKET NAME>",
                "arn:aws:s3:::<BUCKET NAME>/*"
            ]
        }
    ]
}
```
You need to replace `<BUCKET NAME>` with your bucket name that you created. The important thing is to ensure the `Action` list 
shown above to grant the correct access.  

## Configure GitLab to deploy to S3

Now that you created your new GitLab deployer user in Amazon, its time to configure GitLab to use it.  In your GitLab project,
under `Settings > CI / CD > Variables`, we will need to add your `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` so they get 
injected into your Pipelines as environmental variables while they are running.  Make sure to turn on `Masked`, but do not
turn on `State = Protected`.  If protected, the variable is only injected into your protected branches (aka master).  Use
the `ACCESS KEY ID` and `SECRET ACCESS KEY` that you wrote down when you created your user above.  

![Gitlab Variables](gitlab_variables_config.jpg)

You're now all set to allow GitLab to deploy to your Amazon S3 bucket


