# Configure Amazon

Amazon was selected to deploy the system because S3 buckets are a very cheap and efficient way to deploy
static websites.  The Amazon CLI is used by all deployment scripts and must be installed on your local 
machine and/or CI system for this to work.

## Creating your S3 Bucket

First thing, you need to create a new Amazon S3 bucket that will house your deployments. You should take
care when naming your bucket, since Amazon has very specific rules for the name when deploying static
websites.  For example, if your custom domain you will use is called `storybook.borderux.com`, then your
S3 bucket must be called `storybook.borderux.com`.  If you do not name it correctly, then it will not be
able to resolve the URLs correctly and access your S3 bucket.  The fun thing is that using bucket names
with dots in the name prevent us from using Amazon's SSL certificates, so we are stuck being able to only
run http (not https) when accessing your bucket.  

To create a new bucket, do the following:
1. Go to https://console.aws.amazon.com
2. Find the `S3` service
3. Click the `+ Create Bucket` button to create a new bucket
4. Enter your bucket name.  See details above on what to name your bucket
5. Select the Region that will house your bucket.  Note the region because it will be used later
6. You can ignore the `Configure options` step, unless you have specific needs
7. For the `Permissions` step, you will want to select `Block all public access` to `OFF`.  This allows your
bucket to be access from the public internet.
8. Review and create your bucket

You now have a bucket, but it must be configured further in order for it to be used.

## Configure your S3 bucket

Click on your newly created S3 bucket to alter its configuration. 

### Properties

In the Properties tab, select the `Static website hosting` and select `Use this bucket to host a website`.
You will need to specify the Index document to be `index.html` and the Error document to be `error.html`
![S3 Static Website](s3-properties-static-website.jpg)

### Permissions

Under the `Bucket Policy`, you will need to grant public access to your bucket so it can serve static
websites.  
```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::storydemo.borderux.com/*",
        }
    ]
}
```

Under `CORS configuration`, you will want to alter your CORS config to the following
```json
<?xml version="1.0" encoding="UTF-8"?>
<CORSConfiguration xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
  <CORSRule>
    <AllowedOrigin>https://*.amazonaws.com</AllowedOrigin>
    <AllowedMethod>PUT</AllowedMethod>
    <AllowedMethod>POST</AllowedMethod>
    <AllowedMethod>DELETE</AllowedMethod>
    <MaxAgeSeconds>0</MaxAgeSeconds>
    <AllowedHeader>*</AllowedHeader>
  </CORSRule>
  <CORSRule>
    <AllowedOrigin>*</AllowedOrigin>
    <AllowedMethod>GET</AllowedMethod>
    <MaxAgeSeconds>0</MaxAgeSeconds>
    <AllowedHeader>*</AllowedHeader>
  </CORSRule>
</CORSConfiguration>
```

## Accessing your bucket

Once your bucket policy is set to public, you can now access it by going to
`http://<bucket name>.s3-website-<region>.amazonaws.com`, where `<bucket name>` is your bucket
name and `<region>` is your region.  For example, http://storydemo.borderux.com.s3-website-us-west-1.amazonaws.com

## Security

At this point, you should be asking how this secure.  Well, its not very secure since anyone can now
access your bucket on the public URL.  To prevent this, you will want to configure a secure CDN in front of your
S3 bucket and only allow access to it.  At Border, we use CloudFlare, which is an excellent service and provides lots
of free services, such as free HTTPS certificates and free global CDN.  For more information, check out
[Configuring Cloudflare](CONFIG_CLOUDFLARE.md)

## Lifecycle Rules

All your deployed Storybook instances will be put under a single folder called `projects` in your S3 bucket.
The reason for this is that you may want to setup a Lifecycle Rule that runs on that folder.  For example, I set
a rule that cleans up old storybooks after 30 days.  To do this, you select the `Management` tab and then
click the `+ Add lifecycle rule` under the `Lifecycle` tab.  As an example, you can do the following:

1. Under Enter a rule name, put `Cleanup`
2. Under `Expiration`, select `Current Version` under the Configure Expiration and select `Expire current version of object`
to 30 days


