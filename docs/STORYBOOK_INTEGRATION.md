# Storybook Integration

This provides more details on how the system modifies Storybook to achieve its results.

## Authorization

1. The file [manager-head.html](../.storybook/manager-head.html) allows us to inject scripts and styles into the Storybook build.  We 
insert two things:
* A `body::after` tag that covers the entire Storybook window with a white page to hide it
* The Auth0 Single Page App script
* The [auth.js](../.storybook/auth/auth.js) script that has been minified
2. The script [build-storybook-auth.js](../scripts/build-storybook-auth.js) is run after Storybook is built to strip the `<script>` tags from the Storybook HTML, preventing it from loading.  This is stored in a temporary file that is then injected into the `auth.js`
file during the webpack bundling and minification.  The final `auth.js` is then added to the `storybook-static` folder of the Storybook
build or the `public` folder if running Storybook locally.
3. When the deployed Storybook is run, the `auth.js` script is loaded and checks if the user is authorized. If not, it redirects to the
Auth0 universal login page so the user can login.
4. After the user logs in, Auth0 will redirect him back to the root landing page (https://storydemo.borderux.com) instead of back to the
sub-project URL.  To take care of this, a cookie is stored by `auth.js`, which is then read by [auth_redirect.js](../public/auth_redirect.js)
and takes care of redirecting the user back to the appropriate sub-project folder.
5. Once back to the project, `auth.js` script detects the URL parameters and provides them to the Auth0 client to process.
6. If Auth0 accepts the credentials, the auth.js script then checks if the user is authorized for that particular sub-project.  More details about this are provided in [configuring Auth0](CONFIG_AUTH0.md)
7. Once authorized, the auth.js script than adds back the `<script>` tags that were stripped in Step 2 above, which causes Storybook to
load and take-over.  Additionally, the white overlay (`body::after`) is removed and Storybook is exposed.  This happens for both the main `index.html` and `iframe.html` to prevent someone from just loading the iFrame directly.  

## Deployment

Deployment is managed pretty simply using the Amazon CLI.  The scripts [deploy-storybook.js](../scripts/deploy-storybook.js) handles
building the awscli command that is executed to deploy the site.  The process is pretty straightforward

1. Storybook is built using the `yarn build-storybook` command.  This also builds the `auth.js` for authorization and puts it all in the `storybook-static` folder.
2. The [deploy-storybook.js](../scripts/deploy-storybook.js) script executs an `aws s3 sync` command and specifies the Storybook build
folder that should be synced and the path to sync it to.  If the path was already deployed it, it will simply write over the top of the
previous deployment
3. When using [GitLab CD](CONFIG_GITLAB.md), the GitLab runner uses a Docker image that contains Node+Yarn+Python.  The awscli is installed as a `before_script` item, and then GitLab does the same awscli command to deploy.  Of course, GitLab must be setup to have
write access to your S3 bucket for this to work. 