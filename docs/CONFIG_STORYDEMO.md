# Setup your StoryDemo system

Once you have configured all prerequisites, you're ready to setup your own StoryDemo system.  At this point, you can
happily deploy Storybook instances as [sub-projects](CONFIG_SUBPROJECT.md).  If you want a landing page to be shown
such as https://storydemo.borderux.com, you will need to deploy your StoryDemo landing page.  This page is necessary
because of the redirect that occurs after login.  The [auth_redirect.js](../public/auth_redirect.js) file takes care
of redirecting to the appropriate project after a login attempt.

## Edit src files

Feel free to alter/destroy the files in the [src](../src/) folder to create our own landing page.  This is provided
simply as an example for you.

## Setup GitLab CD

This project contains a sample [.gitlab-ci.yml](../.gitlab-ci.yml) for allowing continuous deployment via GitLab.
Read through the information in [Setup GitLab](CONFIG_GITLAB.md) for more details.  This is optional though.  
You can remove the `storydemo-deploy` pipeline configuration from the YML if you want.