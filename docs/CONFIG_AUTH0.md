# Configuring Auth0

[Auth0](https://auth0.com) provides a great, free, and secure authorization service that can be integrated into any web
application very simply.  This system uses the [Single-Page App](https://auth0.com/docs/quickstart/spa/vanillajs) javascript
library integrated into Storybook for security.  This has been added to the Storybook [manager-head.html](../.storybook/manager-head.html) as an inserted script tag, along with the customized [auth.js](../.storybook/auth/auth.js) created for
this system.  

## Create your Auth0 account and tentant

Sign up for your free account with [Auth0](https://auth0.com) and create your first tenant.  The name of the tenant is not
that important, and you can always change it later by creating an new tenant and deleting the first one your created.

## Create your Application

Create your application that will contain your StoryDemo system. For example, just call it `storydemo`.  Select `Single Page Application`.

## Update your auth_config.json

Once your application is created, take the `Domain` and `Client ID` provided and alter the corresponding values in your 
projects [auth_config.json](../.storybook/auth/auth_config.json).  You will also want to update the project `name` while 
you're at it to whatever you want that is correct characters for a URL.

## Alter Auth0 application settings

You will want to alter your application settings in Auth0 to update the following:
* `Allowed Callback URLs`: Set to ```http://localhost:6006, https://<YOUR DOMAIN>```.  The localhost value is used when
running Storybook locally with `yarn storybook`.  Change `<YOUR DOMAIN>` to your domain that will be serving the system.
* `Allowed Web Origins`: Set to ```http://localhost:6006, https://<YOUR DOMAIN>```.  The localhost value is used when
running Storybook locally with `yarn storybook`.  Change `<YOUR DOMAIN>` to your domain that will be serving the system.
* `Allowed Origins (CORS)`: Set to ```https://<YOUR DOMAIN>```

### Authorization using Roles

When users try to access their Storybook, they will be prompted with the Auth0 universal login screen.  If they select
to Sign Up, they can put in their information and grant access, but they will be still denied with an error of
`You must be approved to access this application`.  This is because you must grant access to each user which project (sub-project)
they will have access to.  Ideally, we could configure this directly in Auth0 and grant users access easily.  Using Roles, we
can do this, but it takes a little bit of setup.

### Create a Role

1. Under the `User & Roles` item in the sidebar, select [Roles](https://manage.auth0.com/dashboard/us/border/roles). 
2. For the new Role, give it a great name like `project-<NAME>-access`, where `<NAME>` is the same name you set in your
[package.json](../package.json) storyDemo deploy key.  It does not have to be, but just a recognizable name for
your sub-project.

Once this is created, you will assign this Role to the users that will be granted access to your sub-project.  You can
go to the [Users](https://manage.auth0.com/dashboard/us/border/users) page and select the menu next to each users name and
select `Assign Roles`.  

Even though you do this, you will note that users will still not be able to access their Storybooks.  This is because we 
need to setup a Server Rule to make this all work.

### Create a Server Rule

By default, Auth0 does not provide a lot of information to your client application about the user once he is authorized. 
We want the Roles assigned to the user to come down, but this has to be configured in Auth0 to work.  

1. Select the [Rules](https://manage.auth0.com/dashboard/us/border/rules) item in the sidebar.
2. Select the `+ Create Rule` button
3. For the template, select `Empty rule`
4. Paste the following information into the Script area
```javascript
function (user, context, callback) {
  // Roles should only be set to verified users.
  if (!user.email || !user.email_verified) {
    return callback(null, user, context);
  }
	context.idToken["<customRuleDomain>"] = (context.authorization || {}).roles;
  return callback(null, user, context);
}
```
You will want to replace the `<customRuleDomain>` with your own domain (Ex: `http://storydemo.borderux.com`).  It is not important
what the domain is, given that its a URL domain name format and is unique.  You will also want to update the `customRuleDomain` in
the [auth_config.json](../.storydemo/auth/auth_config.json) to the same value.  

When the Auth0 SPA library function [getUser](https://auth0.github.io/auth0-spa-js/classes/auth0client.html#getuser) is called, the
user information returned will now contain a key such as the following which will contain the array of roles:
```json
{
  "http://storydemo.borderux.com/roles": [],
}
```

## Granting User Access

Once this is created, you will assign this Role to the users that will be granted access to your sub-project.  You can
go to the [Users](https://manage.auth0.com/dashboard/us/border/users) page and select the menu next to each users name and
select `Assign Roles`.  

Additionally, if you modify the [auth_config.js](../.storybook/auth/auth_config.json) and add the users email address to
the `authorized`, they will also be granted access.