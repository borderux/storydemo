# Configuring CloudFlare

[CloudFlare](https://cloudflare.com) is a great service that we use that has a very robust free service.  The best thing that it does is provide free SSL certificates and allows your site to run as https.  Create your free account, and then do the 
following to setup your secure access to your Amazon S3 bucket created in [Configuring Amazon](CONFIGURE_AMAZON.md).

This could also be done with [Amazon's CloudFront](https://aws.amazon.com/cloudfront), which basically does the same thing as 
CloudFlare.  Since we had already configured CloudFlare, and their service is great, we chose to continue with them.

## Create CNAME record

Create a new CNAME record under the `DNS` tab.  At BorderUX, we have our primary website under a CNAME record
with the Name `borderux.com`.  We then created another CNAME record called `storydemo`, which would be subdomain
under our main domain as `storydemo.borderux.com`.  For the Content, specify `s3-website-<region>.amazonaws.com`,
where `<region>` is the region of your Amazon S3 bucket created in [Configuring Amazon](CONFIG_AMAZON.md). 
For example, ours is `s3-website-us-west-1.amazonaws.com`.  This creates a `Proxied` subdomain that is proxied
by CloudFlare. 

## Setup SSL/TLS

You will want to setup SSL certificate for your site to make sure your running over HTTPS.  The best thing is,
this is free with CloudFlare.  You want to make sure you're running in `Full` or `Full (strict)` modes for your
site to ensure that you're running a secure connection to your origin servers.  

If you're testing with your Amazon S3 bucket origin though, you may see CloudFlare complaining that it has connection
problems to your origin. This is because CloudFlare is trying to access your S3 bucket via HTTPS, but Amazon
does not allow that for static website configurations.  This is where CloudFlare Page Rules come into play.

## Setup Page Rules

You will need to create two rules.

1. Create a new rule for changing the SSL connection to `Flexible` just for your storydemo subdomain.  If you are only running Storydemo as your primary domain, then you can change the SSL/TLS mode to `Flexible`.  For us, we are running it as a subdomain
of our primary site, so you will want to create a rule for your subdomain.  For example, the `URL matches` is configured
to be `storydemo.borderux.com/*`.  
> **NOTE**: Make sure to include the `/*` at the end, as its very important
The Settings are: `SSL = Flexible`

2. Create a redirect rule from http->https.  This is only needed if you are using a subdomain, since this can be configured
under `SSL/TLS` globally for your whole site.  For example our rule has `URL matches` is configured to be `storydemo.borderux.com/*` and the Settings are: `Always use HTTPS`

What you have now done is:
* When a user enters your website domain under `http`, it now gets automatically redirected to `https`
* When CloudFlare proxies your site from your Amazon S3 bucket, it can now connect in `Flexible` mode

## Configure Amazon Bucket Access

We now want to make sure that only CloudFlare can access your Amazon S3 bucket and acts as the only front door.  This will force
everyone to go through CloudFlare over a secure HTTPS connection, rather than directly access your S3 bucket.  In order to do this,
we need to alter the Amazon S3 `Bucket Policy`.  Refer back to [Configuring Amazon](CONFIG_AMAZON.md), but modify the `Bucket Policy`
to the following:
```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::storydemo.borderux.com/*",
            "Condition": {
                "IpAddress": {
                    "aws:SourceIp": [
                        "103.21.244.0/22",
                        "103.22.200.0/22",
                        "103.31.4.0/22",
                        "104.16.0.0/12",
                        "108.162.192.0/18",
                        "131.0.72.0/22",
                        "141.101.64.0/18",
                        "162.158.0.0/15",
                        "172.64.0.0/13",
                        "173.245.48.0/20",
                        "188.114.96.0/20",
                        "190.93.240.0/20",
                        "197.234.240.0/22",
                        "198.41.128.0/17",
                        "2400:cb00::/32",
                        "2405:8100::/32",
                        "2405:b500::/32",
                        "2606:4700::/32",
                        "2803:f800::/32",
                        "2c0f:f248::/32",
                        "2a06:98c0::/29"
                    ]
                }
            }
        }
    ]
}
```
What you've now done is blocked all access to your S3 bucket expect from the whitelist of CloudFlare IP addresses.  You will note
that your bucket is no longer `PUBLIC` and you can no longer directly access it anymore through your browser.  This is good, since
this is your security.  If you were using Amazon's CloudFront, you would do the same thing but using Access Lists to specify the
CloudFront access list ID.  

