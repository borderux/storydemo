# Configure Your Sub-Projects

When you are working with a new client or want to create a sub-project in your Storydemo system, you can clone/copy this project if you want to use it a starter, and/or do the following:

1. Copy the `.storybook/auth` folder to your `.storybook` directory
2. Come up with a new URL friendly name for the sub-project.  Alter the `name` in the [package.json](../package.json) storyDemoDeploy key.  You will also need to modify the following:
  * `s3BucketName`: The name of your S3 bucket you will deploy to.  See [Configuring Amazon](CONFIG_AMAZON.md) for more details
  * `region`: The Amazon region where your S3 bucket exists 
3. Create a new `Role` in Auth0 to identify this sub-project.  See details in [Setting up Auth0](CONFIG_AUTH0.md).  Alter the `accessRoleId` in [auth_config.js](../.storydemo/auth0/auth_config.js).  You can also disable authorization for your project by setting `disabled: true`.
4. Copy the [manager-header.html](../.storybook/manager-head.html) to your `.storybook` directory.  This contains the modifications to the Storybook manager necessary for authorization.
5. Copy the `.gitlab-ci.yml` to your project if you would like to use the Gitlab CI/CD configuration
6. Copy the `scripts` folder to your project
7. Alter your projects `package.json` to include the additional scripts used for deployment and storybook

> **NOTE**: The project contains `yarn serve` and `yarn serve-storybook` which use [lite-server](https://github.com/johnpapa/lite-server) for testing deployment.  This is not necessary to bring to your project unless you want to.

## Using Storybook

Using Storybook is no different than the default Storybook behavior.  Use the following scripts:

* `yarn storybook`: Runs a local development server
* `yarn build-storybook`: Builds Storybook as a static app that can be deployed

## Deploying your Storybook

You will need to install the AWS CLI and configure it properly before you can deploy to your Amazon S3 bucket.
For more information, you can read https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html

Using the script command `yarn deploy-storybook --name=<DEPLOYMENT_NAME>` to deploy the current branch instance to your
StoryDemo deployment.  This will create a deployed Storybook in the folder `/project/<PROJECT_NAME>/DEPLOYMENT_NAME`,
where `PROJECT_NAME` is the name of your project defined in [package.json](../package.json) storyDemoDeploy key or the name
of your project in the [package.json](../package.json).

Additionally, you can setup GitLab CD to handle deployment.  See [Setup GitLab Deployment](CONFIG_GITLAB.md) for more information.

