/**
 * Custom error handle to cause a redirect to sub-project folders if a file is not found
 * This is to allow our SPAs to handle errors.  This works by setting a cookie prior to the
 * redirect.  If it redirects, and comes back to this again, then we know its trying to access
 * a file that does not exists, so it shows an error
 */
var _ERROR_COOKIE_NAME_ = "_StoryDemo_errorRedirect_";

function setCookie(name, value, ageInMilliseconds) {
  var expires = "";
  if (ageInMilliseconds) {
    var date = new Date();
    date.setTime(date.getTime() + ageInMilliseconds);
    expires = "; expires=" + date.toUTCString();
  }
  document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

function getCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(";");
  for (var i = 0, len = ca.length; i < len; i++) {
    var c = ca[i];
    while (c.charAt(0) === " ") {
      c = c.substring(1, c.length);
    }
    if (c.indexOf(nameEQ) === 0) {
      return c.substring(nameEQ.length, c.length);
    }
  }
  return null;
}

function eraseCookie(name) {
  document.cookie = name + "=; Max-Age=-99999999;";
}

function getPathname() {
  var pathname = window.location.pathname || "";
  if (pathname[0] === "/") {
    pathname = pathname.substr(1);
  }
  var parts = pathname.split("/");
  if (parts.length && parts[parts.length - 1].indexOf(".") !== -1) {
    parts.splice(-1);
  }
  return parts.join("/") || "";
}

function checkPreviousRedirectCookie(projectPath) {
  var cookie = getCookie(_ERROR_COOKIE_NAME_);
  if (cookie === projectPath) {
    eraseCookie(_ERROR_COOKIE_NAME_);
    return true;
  }
  return false;
}

function redirectTo(projectPath, additionalPath) {
  if (checkPreviousRedirectCookie(projectPath)) {
    return false;
  }
  console.log("StoryDemo redirect: ", projectPath, additionalPath);
  var search = window.location.search || "";
  if (additionalPath) {
    if (additionalPath[0] !== "/") {
      additionalPath = "/" + additionalPath;
    }
    if (!search) {
      search = "?rredirectPath=" + additionalPath;
    } else {
      search += "&rredirectPath=" + additionalPath;
    }
  }
  var fullPath = window.location.origin + "/" + projectPath + "/" + search;
  console.log("Redirecting to: ", fullPath);
  setCookie(_ERROR_COOKIE_NAME_, projectPath, 3000);
  window.location.replace(fullPath);
  return true;
}

function showError() {
  var element = document.getElementById("error");
  if (element) {
    element.innerHTML =
      "<h1>File Not Found</h1><h3>" + window.location.href + "</h3>";
  }
  console.error("StoryDemo error detected");
  console.error("Location not found: ", window.location);
}

window.onload = function() {
  var pathname = getPathname();
  var parts = pathname.split("/");
  if (parts[0].toLowerCase() === "projects" && parts.length > 2) {
    if (redirectTo(parts.slice(0, 3).join("/"), parts.slice(3).join("/"))) {
      return;
    }
  }
  showError();
};
