/*
 *****************************************************************************************
 * Border UX Source File
 * Copyright (C) 2019 Border LLC
 *
 * Handles the redirect after login to the appropriate page based on a stored cookie
 *
 * [CHANGE HISTORY]
 * 1. 2019-Nov-27 (matt@borderux.com) File created
 * 2. 2020-Mar-5 (matt@borderux.com) Modified to redirect if cookie is found always
 *
 *****************************************************************************************
 */

function getCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(";");
  for (var i = 0, len = ca.length; i < len; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1, c.length);
    }
    if (c.indexOf(nameEQ) == 0) {
      return c.substring(nameEQ.length, c.length);
    }
  }
  return null;
}

function eraseCookie(name) {
  document.cookie = name + "=; Max-Age=-99999999;";
}

function main() {
  var queryString = window.location.search;
  var cookieName = "storydemo-redirect-url";
  var url = getCookie(cookieName);
  if (url) {
    eraseCookie(cookieName);
    console.log("StoryDemo auth redirect: ", url);
    window.location.replace(url + queryString);
  } else {
    console.log("StoryDemo: No auth cookie found");
  }
}
main();
